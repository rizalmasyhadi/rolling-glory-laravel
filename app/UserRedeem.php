<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRedeem extends Model
{
    protected $guarded = [];


    public function user()
	{
		return $this->belongsTo(User::class);
	}

    public function gift()
	{
		return $this->belongsTo(Gift::class);
	}
}
