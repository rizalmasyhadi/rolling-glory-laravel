<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gift extends Model
{
    protected $guarded = [];


    public function rating()
	{
		return $this->hasMany(GiftRating::class,  'gift_id', 'id');
	}

    public function redeem()
	{
		return $this->hasMany(UserRedeem::class);
	}
}
