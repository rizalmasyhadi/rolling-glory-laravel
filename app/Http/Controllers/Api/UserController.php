<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Carbon\Carbon;


class UserController extends Controller
{

    public function index(Request $request)
    {
        $page = $request->get('page') ?? 1;
        $perPage = $request->get('per_page') ?? 10;
        $sort = $request->get('sort') ?? 'created_at';
        $order = $request->get('order') ?? 'DESC';

        $users = new User;

        $query = $users->query();

        $query->select('*');

        $lastPage = $query->paginate( $perPage, $page)->lastPage();

        if($lastPage < $page){
            $page = 1;
        }

        $query->orderBy('created_at', $order);

        $query = $query->paginate( $perPage, $page)->appends(request()->all());

        return response()->json($query ,200);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }

        $request['password']=Hash::make($request['password']);

        $user = User::create($request->toArray());

        return response([
                'status' => 'ok',
                'message' => 'Successfully saved', 
                'data' => $user
            ], 200);
        
    }

    public function show($id)
    {
        if( $user = User::find($id) ){
            return response( [
                'status' => 'ok',
                'data' => $user
            ], 200);
        }

        return response(['errors'=> 'No data found'], 422);
    }

    public function edit($id)
    {
        if( $user = User::find($id) ){
            return response( $user, 200);
        }

        return response(['errors'=> 'No data found'], 422);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);

        if( $user = User::find($id) ){
            
            if($request->has('password')){
                $request['password']=Hash::make($request['password']);
            }

            $user->update($request->toArray());

            return response( [
                'status' => 'ok',
                'message' => 'Successfully updated', 
                'data' => $user
            ], 200);
        }

        return response(['errors'=> 'No data found'], 422);
    }


    public function destroy($id)
    {
        if( $user = User::find($id) ){
            $user->delete();
            return response( [
                'status' => 'ok',
                'message' => 'Successfully deleted'
            ], 200);
        }

        return response(['errors'=> 'No data found'], 422);
    }


}