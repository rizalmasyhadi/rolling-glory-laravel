<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Gift;
use App\GiftRating;
use App\UserRedeem;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Carbon\Carbon;

class GiftController extends Controller
{

    public function index(Request $request)
    {
        $page = $request->get('page') ?? 1;
        $perPage = $request->get('per_page') ?? 5;
        $sort = $request->get('sort') ?? 'created_at';
        $order = $request->get('order') ?? 'DESC';

        $gifts = new Gift;

        $query = $gifts->query();

        $query->selectRaw('gifts.*, ( ROUND( AVG( IFNULL(gift_ratings.rating, 0) ) * 2 ) / 2 ) AS rating');
        $query->leftJoin('gift_ratings', 'gift_ratings.gift_id', '=', 'gifts.id');

        $lastPage = $query->paginate( $perPage, $page)->lastPage();

        if($lastPage < $page){
            $page = 1;
        }

        if($request->has('sort')){
            switch ( $request->get('sort') ) {
                case 'newest':
                    $query->orderBy('gifts.created_at', $order);
                    break;
                case 'rating':
                    $query->orderByRaw("AVG(IFNULL(gift_ratings.rating,0)) {$order}");
                    break;
            }

        }else{
            $query->orderBy( 'created_at', $order );
        }
        
        $query->groupBy( 'gifts.id' );
        $query = $query->paginate( $perPage, $page)->appends(request()->all());

        return response()->json($query ,200);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'description' => 'required|string',
        ]);

        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }

        $data = $request->only(['title', 'description', 'stock', 'points']);
        $gift = Gift::create($data);

        return response([
                'status' => 'ok',
                'message' => 'Successfully saved', 
                'data' => $gift
            ], 200);
        
    }

    public function show($id)
    {

        if( $gift = Gift::where('gifts.id', $id) 
                ->selectRaw('gifts.*, IFNULL( ROUND( AVG(gift_ratings.rating) * 2 )  / 2 , 0) AS rating')
                ->leftJoin('gift_ratings', 'gift_ratings.gift_id', '=', 'gifts.id')
                ->groupBy('gifts.id' )->first()
        ){

            return response( [
                'status' => 'ok',
                'data' => $gift
            ], 200);
        }

        return response(['errors'=> 'No data found'], 422);
    }

    public function edit($id)
    {
        if( $gift = Gift::find($id) ){
            return response( $gift, 200);
        }

        return response(['errors'=> 'No data found'], 422);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'description' => 'required|string',
        ]);

        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }

        if( $gift = Gift::find($id) ){
            $data = $request->only(['title', 'description', 'stock', 'points']);
            $gift->update($data);
            return response( [
                'status' => 'ok',
                'message' => 'Successfully updated', 
                'data' => $gift
            ], 200);
        }

        return response(['errors'=> 'No data found'], 422);
    }


    public function destroy($id)
    {
        if( $gift = Gift::find($id) ){
            $gift->delete();
            return response( [
                'status' => 'ok',
                'message' => 'Successfully deleted'
            ], 200);
        }

        return response(['errors'=> 'No data found'], 422);
    }

    public function redeem($id)
    {   
        $user = request()->user();
        $ids = explode(',', $id);

        if( count($ids) > 0 ){

            $outstock = Gift::whereIn('id', $ids )->havingRaw('stock <= ?', [0])->get();
            if( $outstock->count() > 0 ){
                return response( [
                    'status' => 'failed',
                    'message' => 'Some gifts are out of stock',
                    'data' => $outstock,
                    'total' => $outstock->count()
                ], 200);
            }
            
            $gifts = Gift::whereIn('id', $ids )->get();
            foreach ($gifts as $i => $gift ) {
                $redem = UserRedeem::create([
                    'gift_id' => $gift->id,
                    'user_id' => $user->id,
                ]);
                $stock = $gift->stock;
                $stock -- ;
                $gift->update(['stock' => $stock ]);
            }

            return response( [
                'status' => 'ok',
                'message' => 'Successfully redeemed',
                'data' => $gifts
            ], 200);
        }

        return response(['errors'=> 'No data found'], 422);
    }

    public function rating($id, Request $request)
    {
        $user = request()->user();

        $validator = Validator::make($request->all(), [
            'rating' => 'required|integer|between:1,5',
        ]);

        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }

        if( $gift = Gift::with(['rating'])->whereHas('redeem' , function($q) use ($user) {
            return $q->where('user_id', $user->id );
        })->where('id', $id)->first() ){

            if( $rated = GiftRating::where(['gift_id' => $gift->id, 'user_id' => $user->id])->first() ){

                return response( [
                    'status' => 'failed',
                    'message' => 'You have rate this gift previously',
                    'data' => $gift
                ], 200);
            }

            $rating = GiftRating::create([
                'gift_id' => $gift->id,
                'user_id' => $user->id,
                'rating' => $request->rating
            ]);

            return response( [
                'status' => 'ok',
                'message' => 'Successfully rated',
                'data' => $gift
            ], 200);
        }

        return response(['errors'=> 'No data found'], 422);
    }


}
