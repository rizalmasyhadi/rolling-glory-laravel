<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class GiftSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newGifts = [
        	[
        		'title' => 'Samsung Galaxy S9 -Midnight Black 4/64 GB',
        		'description' => 'Ukuran layar: 6.2 inci, Dual Edge Super AMOLED 2960 x 1440 (Quad HD+) 529 ppi, 18.5:9 Memori: RAM 6 GB (LPDDR4), ROM 64 GB, MicroSD up to 400GB Sistem operasi: Android 8.0 (Oreo) CPU: Exynos 9810 Octa-core (2.7GHz Quad + 1.7GHz Quad), 64 bit, 10nm processor Kamera: Super Speed Dual Pixel, 12 MP OIS (F1.5/F2.4 Dual Aperture) + 12MP OIS (F2.4) with LED flash, depan 8 MP, f/1.7, autofocus, 1440p@30fps, dual video call, Auto HDR SIM: Dual SIM (Nano-SIM) Baterai: Non-removable Li-Ion 3500 mAh , Fast Charging on wired and wireless',
        		'stock' => 5,
        	],
        	[
        		'title' => 'Vivo S12',
        		'description' => 'Ukuran layar: IPS LCD, 6,51 inci, 720 x 1600 pixels. Chipset: Mediatek MT6765 Helio P35 (12nm) CPU: Octa-core (4x2.35 GHz Cortex-A53 & 4x1.8 GHz Cortex-A53) GPU: PowerVR GE8320. OS: Android 10, Funtouch 11. RAM: 3 GB. Memori internal: 32 GB.',
        		'stock' => 5,
        	],
        	[
        		'title' => 'OnePlus 7 Pro',
        		'description' => 'OnePlus 7 Pro merupakan handphone HP dengan kapasitas 4000mAh dan layar 6.6" yang dilengkapi dengan kamera belakang 48MP dengan tingkat densitas piksel sebesar 518ppi dan tampilan resolusi sebesar 1440 x 3120pixels. Dengan berat sebesar 206g, handphone HP ini memiliki prosesor Octa Core. Tanggal rilis untuk OnePlus 7 Pro: Mei 2019',
        		'stock' => 5,
        	],
        	[
        		'title' => 'Xiaomi Redmi Note 7 Pro',
        		'description' => 'Layar: IPS LCD capacitive touchscreen, 16M colors, 6.2 inci, terlindungi Corning Gorilla Glass 5. Resolusi layar: 1080 x 2340 pixels, rasio 19:5:9 (~270 ppi density) Chipset: Qualcomm SDM660 Snapdragon 660 (14 nm) OS: Android 9.0 (Pie); MIUI 10.',
        		'stock' => 5,
        	],
        	[
        		'title' => 'Huawei Mate 20 Pro',
        		'description' => 'Image result for Huawei Mate 20 Pro deskripsi Huawei Mate 20 Pro adalah smartphone pertama yang menggunakan chipset HiSilicon Kirin 980 dengan teknologi fabrikasi 7 nanometer (nm) alias yang tercanggih saat ini. Dengan prosesor ini, smartphone akan mengalami peningkatan kecepatan dan daya semakin efisien',
        		'stock' => 5,
        	],
        	[
        		'title' => 'Samsung Galaxy Tab A8 10.5 (2021)',
        		'description' => 'Samsung Galaxy Tab A8 10.5 (2021) dimotori oleh prosesor Unisoc Tiger T618 (12 nm) yang memiliki CPU 8-core dengan konfigurasi 2-core Cortex A75 berkecepatan 2.0 GHz & 6-core Cortex-A55 yang disetel pada kecepatan 2.0 GHz.',
        		'stock' => 5,
        	],

        ];

        DB::table('gifts')->insert($newGifts);

    }
}
