# COMPANY REST GIFTS - LARAVEL 7

---

[Laravel 7](https://laravel.com/)

## Prerequisites

1. PHP >= 7.2.5
2. MySQL or MariaDB
3. [Composer](http://getcomposer.org)


## Auth

1. Oauth2 Laravel/Passport

	```
	{{LocalUrl}}/api/register
	{{LocalUrl}}/api/login
	```

2. Token Type

	```
	Bearer
	```


## Getting Started

1. Clone to your base project directory.

	```
	git clone https://rizalmasyhadi@bitbucket.org/rizalmasyhadi/rolling-glory-laravel.git
	```

2. Install vendor

	```
	composer install
	```

3. Create configuration file `.env` (copy from `.env.example`) and set the database configuration.

	```
	DB_CONNECTION=mysql
	DB_HOST=127.0.0.1
	DB_PORT=3306
	DB_DATABASE=laravel
	DB_USERNAME=root
	DB_PASSWORD=
	```

4. Database migration.

	```
	php artisan migrate
	```


5. Add Laravel Passport and Client 

	```
	php artisan passport:install
	```


6. Seed User and Gifts Data

	```
	php artisan db:seed
	```
	
7. For security reason, please generate new application key.

	```
	php artisan key:generate
	```

8. Run server.

	```
	php artisan serve
	```


## Data Seed

1. User

	```
	name = admin
	email = admin@mail.com
	password = password
	```


## Flow

1. Login

	```
	name = admin
	email = admin@mail.com
	password = password
	url : {{url}}/api/login
	response: token
	```

2. Rest Gifts

	```
	auth = Bearer Token
	url : {{url}}/api/gifts
	pagination params:
	per_page : 5
	page : 1
	sort : rating or newest
	order : asc or desc
	```

3. Rest Users

	```
	auth = Bearer Token
	url : {{url}}/api/users
	```