<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => ['request.json', 'cors']], function () {

    Route::post('/login', 'OAuth\AuthController@login')->name('login.api');
    Route::post('/register','OAuth\AuthController@register')->name('register.api');

    Route::group(['middleware' => 'auth:api' ], function() {

    	Route::get('logout', 'OAuth\AuthController@logout');
    	Route::post('/logout', 'OAuth\AuthController@logout')->name('logout.api');

    	Route::get('user', 'OAuth\AuthController@user');

    	Route::resource('users', 'Api\UserController');

        Route::resource('gifts', 'Api\GiftController');

        Route::post('gifts/{id}/redeem', 'Api\GiftController@redeem');
        Route::post('gifts/{id}/rating', 'Api\GiftController@rating');

    });

});